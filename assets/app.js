import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ReactDom from 'react-dom';
import {Authenticate} from "./components/Authenticate";
import {Home} from "./components/Home";
import PageNotFound from "./components/PageNotFound";

export class App extends React.Component {

    constructor(props){
        super(props);
        this.changeLogApp = this.changeLogApp.bind(this);
        this.changeUnlogFunction = this.changeUnlogFunction.bind(this);

        this.state = {
            isLogged: false
        }
    }

    changeLogApp(){
        this.setState({
            isLogged: true
        });
    }

    changeUnlogFunction(){
        this.setState({
            isLogged: false
        });
    }

    render() {
        return (
            <div>
                { this.state.isLogged ?
                    <Home changeUnlogFunction = {this.changeUnlogFunction} /> :
                    <Authenticate changeLogApp = {this.changeLogApp} /> }
            </div>
        );
    }
}

ReactDom.render(
    <Router>
        <Switch>
            <Route exact path="/" component={App} />
            <Route path="*" component={PageNotFound} />
        </Switch>
    </Router>,
    document.getElementById('root'));
