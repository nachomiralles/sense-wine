import React from "react";

export class Register extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            userName: '',
            userEmail: '',
            userPass: '',
            userPassRepeated: ''
        }
    }

    updateNameValue(evt) {
        this.setState({
            userName: evt.target.value
        });
    }

    updateEmailValue(evt) {
        this.setState({
            userEmail: evt.target.value
        });
    }

    updatePassValue(evt) {
        this.setState({
            userPass: evt.target.value
        });
    }

    updatePassRepeatedValue(evt) {
        this.setState({
            userPassRepeated: evt.target.value
        });
    }

    createPostRequest() {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: this.state.userEmail,
                password: this.state.userPass
            })
        };
        fetch('http://localhost:8000/api/register', requestOptions)
            .then(response => response.json())
            .then(data => {
                if(data['res'] == "OK") this.setState({nowRegistered: true});
            });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.createPostRequest.bind(this)}>

                    <div className="form-group">
                        <label htmlFor="registerInputName">Introduzca su Nombre</label>
                        <input type="text" className="form-control" id="registerInputName"
                               aria-describedby="nameHelp" placeholder="Nacho Miralles"
                               value={this.state.userName}
                               onChange={evt => this.updateNameValue(evt)}
                               required
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="registerInputEmail">Introduzca su Email</label>
                        <input type="email" className="form-control" id="registerInputEmail"
                               aria-describedby="emailHelp" placeholder="usuario@cuatroochenta.com"
                               value={this.state.userEmail}
                               onChange={evt => this.updateEmailValue(evt)}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="registerInputPassword">Introduzca su contraseña</label>
                        <input type="password" className="form-control" id="registerInputPassword"
                               placeholder="Contraseña"
                               value={this.state.userPass}
                               onChange={evt => this.updatePassValue(evt)}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="registerInputPasswordRepeated">Introduzca su contraseña nuevamente</label>
                        <input type="password" className="form-control" id="registerInputPasswordRepeated"
                               placeholder="Repita la contraseña"
                               value={this.state.userPassRepeated}
                               onChange={evt => this.updatePassRepeatedValue(evt)}
                               required
                        />
                    </div>
                    {this.state.userPassRepeated!='' && this.state.userPassRepeated!=this.state.userPass &&
                    <p className="text-danger">Las contraseñas no coinciden.</p>
                    }
                    <button type="submit" className="btn btn-primary">REGISTRARSE</button>


                </form>
            </div>
        );
    }
}