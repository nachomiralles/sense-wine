import React from "react";
import { Link } from 'react-router-dom';

export class Home extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <div>
                <h1>Bienvenido al Home</h1>
                <a href="" onClick={this.props.changeUnlogFunction}>Desconectar.</a>
            </div>
        );
    }
}