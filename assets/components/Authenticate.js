import React from 'react';
import {Login} from "./Login";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import {Register} from "./Register";

export class Authenticate extends React.Component {

    constructor(props){
        super(props);

        this.changeLogAuthenticate = this.changeLogAuthenticate.bind(this);
    }

    changeLogAuthenticate() {
        this.props.changeLogApp();
    }

    render() {
        return (
            <div>
                <div className="d-flex justify-content-center">
                    <div className="w-50 p-3" style= {{'backgroundColor': '#eee'}}>
                        <h3>Bienvenido al Sensor de Vinos.</h3>
                        <Tabs defaultActiveKey="login" id="uncontrolled-tab-example">
                            <Tab eventKey="login" title="Acceder">
                                <Login changeLogAuthenticate = { this.changeLogAuthenticate }/>
                            </Tab>
                            <Tab eventKey="register" title="Resgistrarse">
                                <Register />
                            </Tab>
                        </Tabs>

                    </div>
                </div>
            </div>
        );
    }
}