import React from "react";

export class Login extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            userEmail: '',
            userPass: '',
            loginIncorrect: false
        }
    }

    updateEmailValue(evt) {
        this.setState({
            userEmail: evt.target.value
        });
    }

    updatePassValue(evt) {
        this.setState({
            userPass: evt.target.value
        });
    }

    createPostRequest() {
        if(this.state.userEmail && this.state.userPass) {
            const requestOptions = {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    email: this.state.userEmail,
                    password: this.state.userPass
                })
            };
            fetch('http://localhost:8000/api/login', requestOptions)
                .then(response => response.json())
                // .then(data => console.log("HOLA"));
                .then(data => {
                    if(data['res'] == "BAD") this.setState({loginIncorrect: true});
                    else this.props.changeLogAuthenticate();

                });
        }
    }

    render() {
        return (
            <div>
                <form>
                    <div className="form-group">
                        <label htmlFor="loginInputEmail">Introduzca su Email</label>
                        <input type="email" className="form-control" id="loginInputEmail"
                               aria-describedby="emailHelp" placeholder="usuario@cuatroochenta.com"
                               value={this.state.userEmail}
                               onChange={evt => this.updateEmailValue(evt)}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="loginInputPassword">Introduzca su contraseña</label>
                        <input type="password" className="form-control" id="loginInputPassword"
                               placeholder="Contraseña"
                               value={this.state.userPass}
                               onChange={evt => this.updatePassValue(evt)}
                               required
                        />
                    </div>
                    <button type="button" className="btn btn-primary" onClick={this.createPostRequest.bind(this)}>ACCEDER</button>
                </form>
                <p> {this.state.loginIncorrect}</p>
                {this.state.loginIncorrect &&
                    <p className="text-danger">El usuario o la contraseña no son correctos.</p>
                }
            </div>
        );
    }
}