import React from 'react';

import { Link } from 'react-router-dom';

class PageNotFound extends React.Component{
    render(){
        return(
            <div>
                <h1>UOPS La página buscada no existe, a esto lo llamamos error 404.</h1>
                <Link to="/"> Volver al INICIO </Link>
            </div>
        )
    }
}
export default PageNotFound;