# SENSE WINE TEST PROJECT

Una vez clonado, para que funcione instalar NPM, COMPOSER, SYMFONY y YARN, luego:

*npm install*

*composer install*

*yarn encore dev --watch*

*symfony server:start*

*npm install react-bootstrap*

Para crear la BBDD, en *.env* modificas la linea:

*DATABASE_URL=mysql://root:cuatroochenta@127.0.0.1:3306/sensor_wine_db?serverVersion=5.7"*

por

*DATABASE_URL=mysql://{MI USUARIO}:{MI PASS}@{MI SERVER}/sensor_wine_db?serverVersion=5.7"*

y Ejecutas: 

*php bin/console doctrine:schema:update --force*

Y, si no ha petado nada (sería MUY raro), accedes a:

*localhost:8000*

Y a disfrutar de la WebApp...