<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sensores", mappedBy="user")
     */
    private $sensores;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mediciones", mappedBy="user")
     */
    private $mediciones;

    public function __construct()
    {
        $this->sensores = new ArrayCollection();
        $this->mediciones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Sensores[]
     */
    public function getSensores(): Collection
    {
        return $this->sensores;
    }

    public function addSensore(Sensores $sensore): self
    {
        if (!$this->sensores->contains($sensore)) {
            $this->sensores[] = $sensore;
            $sensore->setUser($this);
        }

        return $this;
    }

    public function removeSensore(Sensores $sensore): self
    {
        if ($this->sensores->removeElement($sensore)) {
            // set the owning side to null (unless already changed)
            if ($sensore->getUser() === $this) {
                $sensore->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Mediciones[]
     */
    public function getMediciones(): Collection
    {
        return $this->mediciones;
    }

    public function addMedicione(Mediciones $medicione): self
    {
        if (!$this->mediciones->contains($medicione)) {
            $this->mediciones[] = $medicione;
            $medicione->setUser($this);
        }

        return $this;
    }

    public function removeMedicione(Mediciones $medicione): self
    {
        if ($this->mediciones->removeElement($medicione)) {
            // set the owning side to null (unless already changed)
            if ($medicione->getUser() === $this) {
                $medicione->setUser(null);
            }
        }

        return $this;
    }
}
