<?php

namespace App\Repository;

use App\Entity\Sensores;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sensores|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sensores|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sensores[]    findAll()
 * @method Sensores[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SensoresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sensores::class);
    }

    // /**
    //  * @return Sensores[] Returns an array of Sensores objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sensores
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
