<?php

namespace App\Repository;

use App\Entity\Variedades;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Variedades|null find($id, $lockMode = null, $lockVersion = null)
 * @method Variedades|null findOneBy(array $criteria, array $orderBy = null)
 * @method Variedades[]    findAll()
 * @method Variedades[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VariedadesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Variedades::class);
    }

    // /**
    //  * @return Variedades[] Returns an array of Variedades objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Variedades
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
