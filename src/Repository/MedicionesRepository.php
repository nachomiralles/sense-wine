<?php

namespace App\Repository;

use App\Entity\Mediciones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Mediciones|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mediciones|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mediciones[]    findAll()
 * @method Mediciones[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MedicionesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mediciones::class);
    }

    // /**
    //  * @return Mediciones[] Returns an array of Mediciones objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Mediciones
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
