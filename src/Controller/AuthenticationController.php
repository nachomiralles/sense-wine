<?php
namespace App\Controller;

use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Json;

class AuthenticationController extends AbstractController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

//    private function showLog(string $text){
//        $this->logger->debug("************************************");
//        $this->logger->debug($text);
//        $this->logger->debug("************************************");
//    }

    /**
     * @Route("/{reactRouting}", name="home", defaults={"reactRouting": null})
     */
    public function index()
    {
        return $this->render('authentication/index.html.twig');
    }

    /**
     * @Route("/api/login", name="login", methods={"POST"})
     */
    public function login(Request $request, UserPasswordEncoderInterface $passwordEncoder): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $db = $this->getDoctrine()->getManager();
        $user = $db->getRepository(User::class)->findOneBy(['email'=>$data['email']]);
        if($user && $user->getPassword() == $data['password']){
            return new JsonResponse(['res' => 'OK']);
        }
        return new JsonResponse(['res' => 'BAD']);

    }

    /**
     * @Route("/api/register", name="register", methods={"POST"})
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $user = new User();
        $db = $this->getDoctrine()->getManager();
        $user->setEmail($data['email']);
//      $user->setPassword($passwordEncoder->encodePassword($user, $data['password']));
        $user->setPassword($data['password']);
        $db->persist($user);
        $db->flush();
        return new JsonResponse(['res' => 'OK']);
    }
}
